# Red-black Tree
[![Build Status](https://travis-ci.org/montao/rbtree2.svg?branch=master)](https://travis-ci.com/montao/rbtree2) 
[![Coverage Status](https://coveralls.io/repos/github/montao/rbtree2/badge.svg?branch=master)](https://coveralls.io/github/montao/rbtree2?branch=master)

Red-black tree with inserts and lookups.
Build with `gcc rbtree.c`

Please use the template provided below, when reporting bugs:
-----

### Expected Behavior

### Observed Behavior

### Bonus Points! Code (or Repository) that Reproduces Issue

### Forensic Information
